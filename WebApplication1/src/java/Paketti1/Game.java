/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Paketti1;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mortti
 */
@Entity
@Table(name = "GAME")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Game.findAll", query = "SELECT g FROM Game g"),
    @NamedQuery(name = "Game.findById", query = "SELECT g FROM Game g WHERE g.id = :id"),
    @NamedQuery(name = "Game.findByGdate", query = "SELECT g FROM Game g WHERE g.gdate = :gdate"),
    @NamedQuery(name = "Game.findByHomegoals", query = "SELECT g FROM Game g WHERE g.homegoals = :homegoals"),
    @NamedQuery(name = "Game.findByAwaygoals", query = "SELECT g FROM Game g WHERE g.awaygoals = :awaygoals"),
    @NamedQuery(name = "Game.findByVenue", query = "SELECT g FROM Game g WHERE g.venue = :venue")})
public class Game implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "GDATE")
    @Temporal(TemporalType.DATE)
    private Date gdate;
    @Column(name = "HOMEGOALS")
    private Integer homegoals;
    @Column(name = "AWAYGOALS")
    private Integer awaygoals;
    @Size(max = 30)
    @Column(name = "VENUE")
    private String venue;
    @JoinColumn(name = "HOME", referencedColumnName = "ID")
    @ManyToOne
    private Team home;
    @JoinColumn(name = "AWAY", referencedColumnName = "ID")
    @ManyToOne
    private Team away;

    public Game() {
    }

    public Game(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getGdate() {
        return gdate;
    }

    public void setGdate(Date gdate) {
        this.gdate = gdate;
    }

    public Integer getHomegoals() {
        return homegoals;
    }

    public void setHomegoals(Integer homegoals) {
        this.homegoals = homegoals;
    }

    public Integer getAwaygoals() {
        return awaygoals;
    }

    public void setAwaygoals(Integer awaygoals) {
        this.awaygoals = awaygoals;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public Team getHome() {
        return home;
    }

    public void setHome(Team home) {
        this.home = home;
    }

    public Team getAway() {
        return away;
    }

    public void setAway(Team away) {
        this.away = away;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Game)) {
            return false;
        }
        Game other = (Game) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Paketti1.Game[ id=" + id + " ]";
    }
    
}
