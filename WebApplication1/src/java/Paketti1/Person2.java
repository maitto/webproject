/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Paketti1;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mortti
 */
@Entity
@Table(name = "person2")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Person2.findAll", query = "SELECT p FROM Person2 p"),
    @NamedQuery(name = "Person2.findById", query = "SELECT p FROM Person2 p WHERE p.id = :id"),
    @NamedQuery(name = "Person2.findByFname", query = "SELECT p FROM Person2 p WHERE p.fname = :fname"),
    @NamedQuery(name = "Person2.findByLname", query = "SELECT p FROM Person2 p WHERE p.lname = :lname"),
    @NamedQuery(name = "Person2.findByBdate", query = "SELECT p FROM Person2 p WHERE p.bdate = :bdate"),
    @NamedQuery(name = "Person2.findByDdate", query = "SELECT p FROM Person2 p WHERE p.ddate = :ddate"),
    @NamedQuery(name = "Person2.findByMother", query = "SELECT p FROM Person2 p WHERE p.mother = :mother"),
    @NamedQuery(name = "Person2.findByFather", query = "SELECT p FROM Person2 p WHERE p.father = :father")})
public class Person2 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 20)
    @Column(name = "FNAME")
    private String fname;
    @Size(max = 25)
    @Column(name = "LNAME")
    private String lname;
    @Column(name = "BDATE")
    @Temporal(TemporalType.DATE)
    private Date bdate;
    @Column(name = "DDATE")
    @Temporal(TemporalType.DATE)
    private Date ddate;
    @Column(name = "MOTHER")
    private Integer mother;
    @Column(name = "FATHER")
    private Integer father;

    public Person2() {
    }

    public Person2(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public Date getBdate() {
        return bdate;
    }

    public void setBdate(Date bdate) {
        this.bdate = bdate;
    }

    public Date getDdate() {
        return ddate;
    }

    public void setDdate(Date ddate) {
        this.ddate = ddate;
    }

    public Integer getMother() {
        return mother;
    }

    public void setMother(Integer mother) {
        this.mother = mother;
    }

    public Integer getFather() {
        return father;
    }

    public void setFather(Integer father) {
        this.father = father;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Person2)) {
            return false;
        }
        Person2 other = (Person2) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Paketti1.Person2[ id=" + id + " ]";
    }
    
}
