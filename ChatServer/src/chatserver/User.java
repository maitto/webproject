import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import static java.lang.System.in;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class User implements Runnable {
    
    
    Scanner reader;
    History history;
    Socket userSocket;
    volatile boolean connected;
    String output;
    BufferedReader in;
    PrintWriter out;
    String nickName;

    public User(Socket s, History h){
        try{
        this.userSocket = s;
        this.history = h;
        this.connected = true;
        this.output = userSocket.getInetAddress().toString() + " connected.";
        in = new BufferedReader(new InputStreamReader(userSocket.getInputStream()));
        out = new PrintWriter(userSocket.getOutputStream(), true);
        nickName = userSocket.getInetAddress().toString();//default nickname is ip adress
        nickName = nickName.substring(1);//delete '/' from beginning of ip adress print
        System.out.println(nickName);
        
    }
        catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public synchronized void run(){ 
        while(this.connected){//loop untill user disconnects 
        
            BufferedReader in = null;
        try {  
            in = new BufferedReader(new InputStreamReader(userSocket.getInputStream()));
            this.output = in.readLine();
            if (this.output == null || this.output.equals("/quit")){//output is null when socket is empty
                //disconnect user when input is null or user types /quit
                System.out.println(userSocket.getInetAddress() +": disconnected");
                this.history.addHistory(" disconnected", nickName);
                this.connected = false;
                history.removeUser(this.userSocket);//remove disconnecting user from userlist of history and server
                userSocket.close();
            }
            else if(this.output.equals("/who")){
                //print all currently connected users
                printMessage("Currently connected (" + history.getUsersHistory().size()+"):");
                for(int i = 0;i < history.getUsersHistory().size();i++){
                    
                printMessage(history.getUsersHistory().get(i).getNick());
                }
            }
            else if(this.output.equals("/rename")){
                //ask user for new username
                printMessage("Write a new nickname: ");
                String oldNick = this.nickName;
                nickName = in.readLine();
                this.history.addHistory(oldNick + " changed name to " + this.nickName,"Server");
                
            }
            else{
                //regular print to server through history(observer pattern)
            this.history.addHistory(this.output, nickName);
            }
            /*
            if(this.output != null){
            System.out.println(userSocket.getInetAddress() +": "+ output);
            }
            else{
                System.out.println(userSocket.getInetAddress() +": disconnected");
                userSocket.close();
            }
                    */
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
}
    
    public String sendMessage (){
         
        return this.output;
}
    
    public String getNick(){
        return this.nickName;
    }
    
    public void changeConnected(boolean s){
        this.connected = s;
    }
    public void printMessage(String s){
        out.println(s);
    }
    
    public boolean getConnected(){
        return this.connected;
    }
    public Socket getSocket(){
        return this.userSocket;
    }
}