
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Server {
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        try {
           
            History history = new History();
            ServerSocket s1 = new ServerSocket(100);
            ArrayList<Thread> threadList = new ArrayList<>();
            ArrayList<User> userList = new ArrayList<>();
            
           
                while(true){
                    try {
                        //wait for connection and create new User object and make thread of it and put into arraylist
                        User newUser = new User(s1.accept(), history);//program hangs here until .accept() goes through
                        userList.add(newUser);
                        history.addUser(newUser);
                        
                        Thread t1 = new Thread(newUser);//making thread
                        threadList.add(t1);//put thread into a list so u can automate thread creation
                        t1.start();//starting thread
                        
                        System.out.println("new user connected");//debugging prints
                        System.out.println(newUser.getNick());
                        
                        for(int i = 0; i<(userList.size()-1);i++){//print who connected to all but newest user (size-1)
                            userList.get(i).printMessage(userList.get(userList.size()-1).getNick()+" connected.");
                        }
                        for(int i = 0; i<history.getHistory().size(); i++){
                            newUser.printMessage(history.getHistory().get(i));//loop through history arraylist and print messages line by line to newly connected user only
                        }
                        System.out.println(newUser.getSocket().getInetAddress());
            } catch (IOException ex) {
                Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            }
              
        }
        }
            catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
            }
            
         
}
    
